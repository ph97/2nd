package huffmant;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class MainClass 
{
	static int encodedTextLength=0;
	static int depth=0;
	
	static class Node 				//the node structure
	{ 
	    int count; 
	    char character; 
	  
	    Node left; 
	    Node right; 
	}
														
	public static HashMap<Character, Integer> findLettersNcount(String text)
	{	
		HashMap<Character, Integer> charsNcount = new HashMap<Character, Integer>(); //hashmap which contains each character as a key and the 
																					 //character's repetitions in the string as a value
		
        for (char character : text.toCharArray())												 //iterating through the characters in the char array
        {
            if(charsNcount.containsKey(character))									 //checks if the hashmap contains the current character
            {
            	charsNcount.put(character, charsNcount.get(character)+1);			 //if the hashmap does contain the character the value of the character is incremented by 1(key=character)
            }
            else
            { 
            	charsNcount.put(character, 1);										 //if the current character is not in the hashmap,the character is put as a key and initialized with value of 1
            }
        }
		return charsNcount;															 //returns the hashmap
	}
	
	public static void printEncodedText(Node root, String s) 
    {  
		if (root.left == null && root.right == null && root.character!='`') 
		{ 
            System.out.println(root.character + ":" + s +" count:"+root.count);
            
            if(depth<s.length())												//finding the tree's depth by comparing the encoded 
            	depth=s.length();												//character's length(the longest encoded=the deepest)
            
            encodedTextLength+=s.length()*root.count;							//calculating the encoded text length by calculating
            return; 															//the current encoded character's length multiplied 
        }																		//by it's repetition count in the string
		
		printEncodedText(root.left, s + "0"); 									//recursively calling the method until the escape case
		printEncodedText(root.right, s + "1"); 									//is true,meanwhile encoding the character by 0 or 1
    } 
	
	public static void main(String[] args)
	{
		String text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
	    HashMap<Character, Integer> charsNfreq=findLettersNcount(text);
	    int length=	charsNfreq.size(); 									//hashmap's length
	    
	    
		PriorityQueue<Node> q= new PriorityQueue<Node>(length, (n1,n2) -> n1.count-n2.count);
		
		for (Map.Entry<Character,Integer> entry: charsNfreq.entrySet()) {
            Node node = new Node(); 					//creating a node
            
            node.character =entry.getKey(); 		//setting the node character field to be equal to the current character in the loop
            node.count = entry.getValue();			//setting the node count field to be equal to the current count in the loop
            
            q.add(node); 								//adds the node to the queue
			
		}		
        Node root = null; 								//creating a root node 
  
        while (q.size() > 1) 							//goes through the queue as long as the size is greater than 1
        { 
        	Node x = q.poll(); 							//returns the first node and removes it from the queue
  
            Node y = q.poll(); 
    
            Node f = new Node(); 						//creating a new node 
  
            f.count = x.count + y.count; 				//setting the new node count field to be equal to the sum of the previous two nodes , which were removed from the queue
            f.character = '`'; 							//setting the new node character to be equal to nothing (`)
  
            f.left = x; 								//setting the new node reference to the left to be equal to the first node which we removed from the queue in this iteration
            f.right = y; 								//setting the new node reference to the right to be equal to the second node which we removed from the queue in this iteration
            root = f; 									//setting the root to be equal to the new node,which we just created
            q.add(f); 									//adds the new node to the queue
        } 
		
        printEncodedText(root, "");						//printing the encoded characters and their repetition's count 
        System.out.println("\nencoded text length:"+encodedTextLength);
        System.out.println("tree's depth:"+(depth+1));
        
	} 
}
